using System;

namespace MovieScore.Infra.CrossCutting.Helpers
{
    public interface ITokenHelper
    {
        string GenerateToken(Guid userId, string role, string secret);
        Guid GetLoggedUserId();
    }
}
