﻿using Microsoft.EntityFrameworkCore;
using MovieScore.Domain.Entities;
using MovieScore.Infra.Data.Mappings;

namespace MovieScore.Infra.Data.Context
{
    public class MovieScoreContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Evaluation> Evaluations { get; set; }

        public MovieScoreContext(DbContextOptions<MovieScoreContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("MovieScore");

            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new CustomerMap());
            modelBuilder.ApplyConfiguration(new MovieMap());
            modelBuilder.ApplyConfiguration(new EvaluationMap());
        }
    }
}
