﻿using System.Collections.Generic;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Infra.Data.Context;
using MovieScore.Infra.Data.Repositories.Base;

namespace MovieScore.Infra.Data.Repositories
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(MovieScoreContext context) : base(context) { }

        public ICollection<Movie> GetWithEvaluations()
        {
            return GetAll(x => x.Evaluations);
        }
    }
}
