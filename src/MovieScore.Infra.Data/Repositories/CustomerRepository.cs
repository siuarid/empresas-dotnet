﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Infra.Data.Context;
using MovieScore.Infra.Data.Repositories.Base;

namespace MovieScore.Infra.Data.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(MovieScoreContext context) : base(context) {  }

        public Guid GetId(Guid userId)
        {
            return _context.Set<Customer>()
                .AsNoTracking()
                .Where(customer => customer.UserId == userId)
                .Select(customer => customer.Id)
                .First();
        }

        public Customer GetNoTracking(Guid id)
        {
            return _context.Set<Customer>()
                .AsNoTracking()
                .FirstOrDefault(customer => customer.Id == id && customer.User.Active);
        }
    }
}
