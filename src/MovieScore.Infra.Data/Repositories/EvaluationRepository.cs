﻿using System;
using System.Collections.Generic;
using System.Text;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Infra.Data.Context;
using MovieScore.Infra.Data.Repositories.Base;

namespace MovieScore.Infra.Data.Repositories
{
    public class EvaluationRepository : Repository<Evaluation>, IEvaluationRepository
    {
        public EvaluationRepository(MovieScoreContext context) : base(context) { }
    }
}
