﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieScore.Domain.Entities;

namespace MovieScore.Infra.Data.Mappings
{
    public class CustomerMap : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(x => x.Id)
                .HasColumnName("customer_id");

            builder.Property(x => x.UserId)
                .HasColumnName("user_id");

            builder.Property(x => x.Name);
            builder.Property(x => x.BirthDate);

            builder.HasOne(x => x.User)
                .WithOne(x => x.Customer)
                .HasForeignKey<Customer>(x => x.UserId);
        }
    }
}
