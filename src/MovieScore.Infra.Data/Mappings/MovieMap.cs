﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieScore.Domain.Entities;

namespace MovieScore.Infra.Data.Mappings
{
    public class MovieMap : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.ToTable("Movie");

            builder.Property(x => x.Id)
                .HasColumnName("movie_id");

            builder.Property(x => x.Title);
            builder.Property(x => x.RunningTime);
            builder.Property(x => x.Director);
            builder.Property(x => x.Gender);
            builder.Property(x => x.ProductionCompany);
            builder.Property(x => x.ReleaseDate);
        }
    }
}
