﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MovieScore.Application.Services.Concretes;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Notifications;
using MovieScore.Domain.Services;
using MovieScore.Infra.CrossCutting.Bus;
using MovieScore.Infra.CrossCutting.Helpers;
using MovieScore.Infra.Data.Context;
using MovieScore.Infra.Data.Repositories;

namespace MovieScopre.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Context
            services.AddScoped<MovieScoreContext>();

            //Bus
            services.AddScoped<IBus, InMemoryBus>();

            //Helpers
            services.AddScoped<ITokenHelper, TokenHelper>();

            //Events
            services.AddScoped<IDomainNotificationHandler<DomainNotification>, DomainNotificationHandler>();

            //Repositories
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IEvaluationRepository,EvaluationRepository>();

            //AppServices
            services.AddScoped<IAuthenticationAppService, AuthenticationAppService>();
            services.AddScoped<ICustomerAppService, CustomerAppService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IMovieAppService, MovieAppService>();

            //Services
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IMovieService, MovieService>();
        }
    }
}
