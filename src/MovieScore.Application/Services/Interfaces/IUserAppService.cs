﻿using System;
using System.Collections.Generic;
using System.Text;
using MovieScore.Application.Services.Interfaces.Base;
using MovieScore.Application.ViewModels.User.Request;
using MovieScore.Application.ViewModels.User.Response;

namespace MovieScore.Application.Services.Interfaces
{
    public interface IUserAppService : IAppServiceBase
    {
        ICollection<UserCommonViewModel> GetCommon(int? pageNumber = null, int? pageSize = null);
        void Insert(UserInsertViewModel viewModel);
        void Update(UserUpdateViewModel viewModel);
    }
}
