﻿using System;
using System.Collections.Generic;
using MovieScore.Application.Services.Interfaces.Base;
using MovieScore.Application.ViewModels.Movie.Evaluation.Request;
using MovieScore.Application.ViewModels.Movie.Request;
using MovieScore.Application.ViewModels.Movie.Response;

namespace MovieScore.Application.Services.Interfaces
{
    public interface IMovieAppService : IAppServiceBase
    {
        void Insert(MovieInsertViewModel viewModel);
        void InsertEvaluation(EvaluationInsertViewModel viewModel);
        ICollection<MovieViewModel> GetAll();
        MovieViewModel Get(Guid id);
        MovieViewModel GetByTitle(string title);
    }
}
