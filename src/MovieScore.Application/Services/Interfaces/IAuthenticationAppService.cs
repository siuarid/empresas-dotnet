﻿using MovieScore.Application.Services.Interfaces.Base;
using MovieScore.Application.ViewModels.Auth;

namespace MovieScore.Application.Services.Interfaces
{
    public interface IAuthenticationAppService : IAppServiceBase
    {
        LoginResponseViewModel Login(LoginViewModel viewModel, string secret);
    }
}
