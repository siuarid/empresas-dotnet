﻿using AutoMapper;
using MovieScore.Application.Services.Interfaces.Base;

namespace MovieScore.Application.Services.Concretes.Base
{
    public class AppServiceBase : IAppServiceBase
    {
        protected readonly IMapper _mapper;
        //private readonly IBus _bus;

        protected AppServiceBase(IMapper mapper)
        {
            _mapper = mapper;
            //_bus = bus;
        }
    }
}
