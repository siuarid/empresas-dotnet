﻿using System;
using AutoMapper;
using MovieScore.Application.Services.Concretes.Base;
using MovieScore.Application.Services.Interfaces;
using MovieScore.Application.ViewModels.Customer.Requests;
using MovieScore.Application.ViewModels.Customer.Responses;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Services;

namespace MovieScore.Application.Services.Concretes
{
    public class CustomerAppService : AppServiceBase, ICustomerAppService
    {
        private readonly ICustomerService _customerService;

        public CustomerAppService(ICustomerService customerService, IMapper mapper) : base(mapper)
        {
            _customerService = customerService;
        }

        public UserViewModel Insert(CustomerInsertViewModel viewModel)
        {
            var customer = _mapper.Map<Customer>(viewModel);

            return _mapper.Map<UserViewModel>(_customerService.Insert(customer));
        }

        public CustomerVIewModel Update(CustomerUpdateViewModel viewModel)
        {
            var customer = _mapper.Map<CustomerUpdateCommand>(viewModel);

            return _mapper.Map<CustomerVIewModel>(_customerService.Update(customer));
        }
    }
}
