﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieScore.Application.ViewModels.Movie.Evaluation.Request
{
    public class EvaluationInsertViewModel
    {
        public Guid CustomerId { get; set; }
        public Guid MovieId { get; set; }
        [Range(0, 4, ErrorMessage = "Rate must be between 0 and 4")]
        public int Rate { get; set; }
    }
}
