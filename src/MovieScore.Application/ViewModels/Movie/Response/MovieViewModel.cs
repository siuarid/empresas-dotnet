﻿using System;

namespace MovieScore.Application.ViewModels.Movie.Response
{
    public class MovieViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int RunningTime { get; set; }
        public string Director { get; set; }
        public string Gender { get; set; }
        public string ProductionCompany { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Rate { get; set; }
    }
}
