﻿using MovieScore.Domain.Enums;

namespace MovieScore.Application.ViewModels.User.Request
{
    public class UserInsertViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public Role Role { get; set; }
    }
}
