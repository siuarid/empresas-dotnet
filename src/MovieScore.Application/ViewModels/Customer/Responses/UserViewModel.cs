﻿namespace MovieScore.Application.ViewModels.Customer.Responses
{
    public class UserViewModel
    {
        public string Login { get; set; }
        public string Email { get; set; }
    }
}
