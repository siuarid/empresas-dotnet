﻿using MovieScore.Domain.Enums;

namespace MovieScore.Application.ViewModels.Customer.Requests
{
    public class CommonUserInsertVIewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
