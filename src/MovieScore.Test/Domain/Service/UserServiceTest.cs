using System;
using System.Linq.Expressions;
using Moq;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Enums;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Notifications;
using MovieScore.Domain.Services;
using Xunit;

namespace MovieScore.Test.Domain.Service
{
    public class UserServiceTest
    {
        private readonly UserService _userService;
        private readonly Mock<IUserRepository> _repositoryMock;
        private readonly Mock<IBus> _busMock;

        public UserServiceTest()
        {
            _repositoryMock = new Mock<IUserRepository>();
            _busMock = new Mock<IBus>();
            _userService = new UserService(_repositoryMock.Object, _busMock.Object);
        }

        [Fact]
        public void InsertUser_ValidUser_Success()
        {
            //Arrange
            var user = new User("login", "email@email.com", "password", Role.CommonUser);

            _repositoryMock.Setup(x => x.Get(x => (x.Login == user.Login || x.Email == user.Email) && x.Active)).Returns((User)null);

            //Act
            _userService.Insert(user);

            //Assert
            _repositoryMock.Verify(x => x.Add(user), Times.Once);
            _repositoryMock.Verify(x => x.Save(), Times.Once);
        }

        [Fact]
        public void InsertUser_EmailAlreadyExists_ShouldNotifyError()
        {
            //Arrange
            var user = new User("login", "email@email.com", "password", Role.CommonUser);

            _repositoryMock.Setup(x => x.Get(It.IsAny<Expression<Func<User, bool>>>())).Returns(user);
            _busMock.Setup(x => x.RaiseEvent(It.IsAny<DomainNotification>()));

            //Act
            _userService.Insert(user);

            //Assert
            _busMock.Verify(x => x.RaiseEvent(It.IsAny<DomainNotification>()), Times.Once);
            _repositoryMock.Verify(x => x.Add(user), Times.Never);
            _repositoryMock.Verify(x => x.Save(), Times.Never);
        }
    }
}
