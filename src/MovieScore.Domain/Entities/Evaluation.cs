using System;
using MovieScore.Domain.Entities.Base;

namespace MovieScore.Domain.Entities
{
    public class Evaluation : Entity<Evaluation>
    {
        public Guid CustomerId { get; private set; }
        public Guid MovieId { get; private set; }
        public int Rate { get; private set; }
        public DateTime EvaluationDate { get; set; }

        public Customer Customer { get; private set; }
        public Movie Movie { get; private set; }

        public Evaluation() { }

        public Evaluation(Guid customerId, Guid movieId, int rate)
        {
            CustomerId = customerId;
            MovieId = movieId;
            Rate = rate;
            EvaluationDate = DateTime.Now;
        }
    }
}
