﻿using System;
using System.Collections.Generic;
using System.Text;
using MovieScore.Domain.Entities.Base;
using MovieScore.Domain.Enums;
using MovieScore.Infra.CrossCutting.Helpers;

namespace MovieScore.Domain.Entities
{
    public class User : Entity<User>
    {
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public bool Active { get; private set; }
        public Role Role { get; private set; }

        public Customer Customer { get; private set; }
        public ICollection<Evaluation> Evaluations { get; private set; }

        //TODO:Implments crypt password
        public User(Guid id, string login, string email, string password, Role role)
        {
            Id = id;
            Login = login;
            Email = email;
            Password = password;
            Role = role;
            Active = true;
        }

        public User(string login, string email, string password, Role role)
        {
            Login = login;
            Email = email;
            Password = password;
            Role = role;
            Active = true;
        }

        public void Disable()
        {
            Active = false;
        }
    }
}
