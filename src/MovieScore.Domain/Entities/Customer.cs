﻿using System;
using System.Collections.Generic;
using MovieScore.Domain.Entities.Base;

namespace MovieScore.Domain.Entities
{
    public class Customer : Entity<Customer>
    {
        public Guid UserId { get; private set; }
        public string Name { get; private set; }
        public DateTime BirthDate { get; private set; }

        public User User { get; private set; }
        public ICollection<Evaluation> Evaluations { get; private set; }
        
        public Customer() { }
        public Customer(Guid id, string name, DateTime birthDate, Guid userId)
            => (Id, Name, BirthDate, UserId) = (id, name, birthDate, userId);
    }
}
