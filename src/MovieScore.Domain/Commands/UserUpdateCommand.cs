﻿using System;
using System.Collections.Generic;
using System.Text;
using MovieScore.Domain.Enums;

namespace MovieScore.Domain.Commands
{
    public class UserUpdateCommand
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
    }
}
