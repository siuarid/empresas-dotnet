﻿namespace MovieScore.Domain.Enums
{
    public enum Role
    {
        Admin,
        CommonUser
    }
}
