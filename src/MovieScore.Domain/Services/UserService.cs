using System;
using System.Collections.Generic;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Services.Base;

namespace MovieScore.Domain.Services
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository, IBus bus) : base(bus)
        {
            _userRepository = userRepository;
        }

        public ICollection<User> GetCommon(int? pageNumber = null, int? pageSize = null)
        {
            return _userRepository.GetCommon(pageNumber, pageSize);
        }

        public void Insert(User user)
        {
            if(_userRepository.Get(x => (x.Login == user.Login || x.Email == user.Email) && x.Active) != null)
            {
                NotifyValidationError("User already exists");
                return;
            }

            _userRepository.Add(user);
            _userRepository.Save();
        }

        public void Update(UserUpdateCommand message)
        {
            var existingUser = _userRepository.GetById(message.Id);

            if(existingUser == null)
            {
                NotifyValidationError("User doesn't exists");
                return;
            }

            var userUpdate = new User(message.Id, message.Login, message.Email, message.Password, message.Role);

            _userRepository.Update(userUpdate);
            _userRepository.Save();
        }

        public void Disable(Guid id)
        {
            var user = _userRepository.GetById(id);

            if(user == null)
            {
                NotifyValidationError("User doesn't exists");
                return;
            }

            user.Disable();
            _userRepository.Update(user);
            _userRepository.Save();
        }
    }
}
