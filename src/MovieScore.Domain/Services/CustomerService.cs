﻿using System;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Services.Base;
using MovieScore.Infra.CrossCutting.Helpers;

namespace MovieScore.Domain.Services
{
    public class CustomerService : ServiceBase, ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITokenHelper _tokenHelper;

        public CustomerService(ICustomerRepository customerRepository, IUserRepository userRepository, ITokenHelper tokenHelper, IBus bus) : base(bus)
        {
            _customerRepository = customerRepository;
            _userRepository = userRepository;
            _tokenHelper = tokenHelper;
        }

        public User Insert(Customer customer)
        {
            if(customer.User.Role != Enums.Role.CommonUser)
            {
                NotifyValidationError("Customer should has a common user role");
                
                return null;
            }

            if(_userRepository.Get(x => x.Login == customer.User.Login) != null)
            {
                NotifyValidationError("User already exists");

                return null;
            }

            _customerRepository.Add(customer);
            _customerRepository.Save();

            return customer.User;
        }

        public Customer Update(CustomerUpdateCommand message)
        {
            var existingCustomer = _customerRepository.GetNoTracking(message.Id);
            
            if(existingCustomer == null)
            {
                NotifyValidationError("Customer doesn't exists");
                return null;
            }

            var custommer = new Customer(message.Id, message.Name, message.BirthDate, existingCustomer.UserId);

            _customerRepository.Update(custommer);
            _customerRepository.Save();

            return custommer;
        }
    }
}
