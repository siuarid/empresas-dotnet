﻿using System;
using System.Collections.Generic;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories;
using MovieScore.Domain.Interfaces.Services;
using MovieScore.Domain.Services.Base;

namespace MovieScore.Domain.Services
{
    public class MovieService : ServiceBase, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IEvaluationRepository _evaluationRepository;

        public MovieService(IMovieRepository movieRepository, IEvaluationRepository evaluationRepository, IBus bus) : base(bus)
        {
            _movieRepository = movieRepository;
            _evaluationRepository = evaluationRepository;
        }

        public Movie Get(Guid id)
        {
            return _movieRepository.GetById(id);
        }

        public ICollection<Movie> GetAll()
        {
            return _movieRepository.GetWithEvaluations();
        }

        public Movie GetByTitle(string title)
        {
            return _movieRepository.Get(x => x.Title.ToUpper() == title.ToUpper());
        }

        public void Insert(Movie movie)
        {
            _movieRepository.Add(movie);
            _movieRepository.Save();
        }

        public void InsertEvaluation(Evaluation evaluation)
        {
            if(_movieRepository.GetById(evaluation.MovieId) == null)
            {
                NotifyValidationError("Movie not found");
                return;
            }

            _evaluationRepository.Add(evaluation);
            _evaluationRepository.Save();
        }
    }
}
