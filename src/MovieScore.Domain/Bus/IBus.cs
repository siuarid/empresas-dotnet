﻿using MovieScore.Domain.Events;

namespace MovieScore.Domain.Bus
{
    public interface IBus
    {
        void RaiseEvent<T>(T theEvent) where T : Event;
    }
}
