﻿using System.Collections.Generic;
using MovieScore.Domain.Events;

namespace MovieScore.Domain.Interfaces.Notifications
{
    public interface IDomainNotificationHandler<T> : IHandler<T> where T : Message
    {
        bool HasNotifications();
        List<T> GetNotifications();
    }
}
