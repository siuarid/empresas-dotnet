using System;
using System.Collections.Generic;
using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;

namespace MovieScore.Domain.Interfaces.Services
{
    public interface IUserService
    {
        void Insert(User user);
        void Update(UserUpdateCommand message);
        ICollection<User> GetCommon(int? pageNumber = null, int? pageSize = null);
    }
}
