﻿using MovieScore.Domain.Commands;
using MovieScore.Domain.Entities;

namespace MovieScore.Domain.Interfaces.Services
{
    public interface ICustomerService
    {
        User Insert(Customer customer);
        Customer Update(CustomerUpdateCommand customer);
    }
}
