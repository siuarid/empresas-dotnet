using System;
using MovieScore.Domain.Entities;

namespace MovieScore.Domain.Interfaces.Services
{
    public interface IAuthenticationService
    {
        User Authenticate(string login, string password);
    }
}
