﻿using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories.Base;

namespace MovieScore.Domain.Interfaces.Repositories
{
    public interface IEvaluationRepository : IRepository<Evaluation>
    {
    }
}
