using System;
using System.Collections.Generic;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories.Base;

namespace MovieScore.Domain.Interfaces.Repositories
{
    public interface IMovieRepository : IRepository<Movie>
    {
        ICollection<Movie> GetWithEvaluations();
    }
}
