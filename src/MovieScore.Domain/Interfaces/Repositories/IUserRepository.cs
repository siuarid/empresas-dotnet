﻿using System;
using System.Collections.Generic;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories.Base;

namespace MovieScore.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetActive(string login);
        ICollection<User> GetCommon(int? pageNumber = null, int? pageSize = null);
    }
}
