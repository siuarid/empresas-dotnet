﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MovieScore.Domain.Entities.Base;

namespace MovieScore.Domain.Interfaces.Repositories.Base
{
    public interface IRepository<TEntity> where TEntity : Entity<TEntity>
    {
        Guid Add(TEntity entity);
        TEntity GetById(Guid id, params Expression<Func<TEntity, object>>[] include);
        ICollection<TEntity> GetAll(params Expression<Func<TEntity, object>>[] include);
        ICollection<TEntity> Find(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        void Update(TEntity entity);
        void Remove(Guid id);
        void Remove(TEntity entity);
        void Save();
    }
}
