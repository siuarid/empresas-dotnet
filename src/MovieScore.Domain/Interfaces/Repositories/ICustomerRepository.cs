﻿using System;
using MovieScore.Domain.Entities;
using MovieScore.Domain.Interfaces.Repositories.Base;

namespace MovieScore.Domain.Interfaces.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Customer GetNoTracking(Guid id);
        Guid GetId(Guid userId);
    }
}
