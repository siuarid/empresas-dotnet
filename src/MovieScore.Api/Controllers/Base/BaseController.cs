﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieScore.Domain.Bus;
using MovieScore.Domain.Interfaces.Notifications;
using MovieScore.Domain.Notifications;

namespace MovieScore.Api.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : Controller
    {

        protected readonly IDomainNotificationHandler<DomainNotification> Notifications;
        protected readonly IBus Bus;

        public BaseController(IBus bus, IDomainNotificationHandler<DomainNotification> notifications)
        {
            Bus = bus;
            Notifications = notifications;
        }

        protected void InvalidViewModelNotify()
        {
            var erros = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var erro in erros)
            {
                var erroMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
                NotifyError(string.Empty, erroMsg);
            }
        }

        protected void NotifyError(string codigo, string mensagem)
        {
            Bus.RaiseEvent(new DomainNotification(codigo, mensagem));
        }

        protected IActionResult Response(object result = null)
        {
            return Response<object>(result);
        }

        protected IActionResult Response<T>(T result)
        {
            if (ValidOperation())
            {
                return Ok(new ReturnContentJson<T>(true, result));
            }

            var instance = Activator.CreateInstance<T>();

            return BadRequest(new ReturnContentJson<T>(false, instance, Notifications.GetNotifications().Select(n => n.Value)));
        }

        protected bool ValidOperation()
        {
            return !Notifications.HasNotifications();
        }
    }
}
